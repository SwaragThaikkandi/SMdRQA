Cite us
=======

.. hint::

   You can cite the package using its DOI here: 
  .. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.10854678.svg
    :target: https://doi.org/10.5281/zenodo.10854678
Main publication
----------------

You can cite the **main publication** for *SMdRQA* as follows:

- Thaikkandi, Swarag, and K. M. Sharika. "Analyzing time series of unequal durations using Multidimensional Recurrence Quantification Analysis (MdRQA): validation and implementation using Python." arXiv preprint arXiv:2307.11675 (2023).

.. code-block:: none
   :caption: The bibtex citation information for SMdRQA

    @article{thaikkandi2023analyzing,
        title={Analyzing time series of unequal durations using Multidimensional Recurrence Quantification Analysis (MdRQA): validation and implementation using Python},
        author={Thaikkandi, Swarag and Sharika, KM},
        journal={arXiv preprint arXiv:2307.11675},
        year={2023}
    }


Other related papers
---------------------

- 'Sharika, K. M., Swarag Thaikkandi, and Michael L. Platt. "Interpersonal heart rate synchrony predicts effective group performance in a naturalistic collective decision-making task." bioRxiv (2023)'. <https://doi.org/10.1101%2F2023.07.24.550277>`_
