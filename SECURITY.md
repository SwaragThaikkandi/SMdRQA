# Security Policy

## Supported Versions

Currently, our project is provided under the GNU Public License and is available in versions "0.1.0" and "2024.3.22". Please note that no versions of our project are supported by security updates by default.

| Version  | Supported          |
| -------- | ------------------ |
| 0.1.0    | :x:                |
| 2024.3.22| :x:                |
| 2024.3.23| :x:                |
| 2024.3.24| :x:                |


## Reporting a Vulnerability

We take security seriously, even though our project does not access the internet, store or send user data, or provide any security updates by default. If you discover a vulnerability or have security concerns, we encourage you to report it promptly. Hoerver we uses tools such as [Bandit](https://github.com/PyCQA/bandit), and semantic code analysis engine such as [CodeQL](https://codeql.github.com/). In addition, we also publish [Dependency review](https://docs.github.com/en/code-security/supply-chain-security/understanding-your-software-supply-chain/about-dependency-review) for each pull request, which inform about insecure dependencies before you introduce them to your environment, and provides information on license, dependents, and age of dependencies.

Please visit our [discussion forum](https://github.com/SwaragThaikkandi/SMdRQA/discussions) to report any vulnerabilities. Our team will review the reported vulnerabilities, but please note that we do not provide regular security updates or fixes for reported issues. However, we appreciate your contribution to the security of our project and will do our best to address any reported concerns in a timely manner.

