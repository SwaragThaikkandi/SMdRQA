---
# SMdRQA: Implementing Sliding Window MdRQA to get Summary Statistics Estimate of MdRQA measures from the Data
---

[![image](https://zenodo.org/badge/DOI/10.5281/zenodo.10854678.svg)](https://doi.org/10.5281/zenodo.10854678)
[![image](https://img.shields.io/github/license/SwaragThaikkandi/SMdRQA)](https://img.shields.io/github/license/SwaragThaikkandi/SMdRQA/LICENSE)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)


## Package integrity

[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/python-package.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/python-package.yml)
[![pages-build-deployment](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/pages/pages-build-deployment/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/pages/pages-build-deployment)

## PyPI Package Information

[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/python-publish.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/python-publish.yml)
[![image](https://img.shields.io/pypi/v/SMdRQA)](https://pypi.org/project/SMdRQA/)
[![image](https://img.shields.io/pypi/pyversions/SMdRQA.svg?logo=python&logoColor=FFE873)](https://pypi.python.org/pypi/SMdRQA)
[![image](https://img.shields.io/pypi/format/SMdRQA)](https://pypi.org/project/SMdRQA/)
[![image](https://img.shields.io/pypi/status/SMdRQA)](https://pypi.org/project/SMdRQA/)
[![image](https://img.shields.io/pypi/implementation/SMdRQA)](https://pypi.org/project/SMdRQA/)
[![image](https://img.shields.io/pypi/wheel/SMdRQA)](https://pypi.org/project/SMdRQA/)
[![image](https://static.pepy.tech/badge/SMdRQA)](https://pepy.tech/project/SMdRQA)

## Security

[![OpenSSF Scorecard](https://api.securityscorecards.dev/projects/github.com/SwaragThaikkandi/SMdRQA/badge)](https://securityscorecards.dev/viewer/?uri=github.com/SwaragThaikkandi/SMdRQA)
[![Synk- Package Health](https://snyk.io//advisor/python/SMdRQA/badge.svg)](https://snyk.io//advisor/python/SMdRQA)
[![Scorecards supply-chain security](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/scorecard.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/scorecard.yml)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/github-code-scanning/codeql/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/github-code-scanning/codeql)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/bandit.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/bandit.yml)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/dependency-review.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/dependency-review.yml)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/devskim.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/devskim.yml)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/ossar.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/ossar.yml)


## GitHub

[![image](https://img.shields.io/github/v/release/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/releases)
[![image](https://img.shields.io/github/issues-pr/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/pulls)
[![image](https://img.shields.io/github/stars/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/stargazers)
[![image](https://img.shields.io/github/languages/count/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/languages)
[![image](https://img.shields.io/github/search/:user/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/search)
[![image](https://img.shields.io/github/languages/top/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA)
[![image](https://img.shields.io/github/languages/code-size/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA)
[![image](https://img.shields.io/github/directory-file-count/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA)
[![image](https://img.shields.io/github/repo-size/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA)
[![image](https://img.shields.io/gitlab/pipeline-status/SwaragThaikkandi/SMdRQA)](https://gitlab.com/SwaragThaikkandi/SMdRQA/pipelines)
[![image](https://img.shields.io/gitlab/pipeline-coverage/SwaragThaikkandi/SMdRQA)](https://gitlab.com/SwaragThaikkandi/SMdRQA/pipelines)
[![image](https://img.shields.io/github/downloads/SwaragThaikkandi/SMdRQA/total)](https://github.com/SwaragThaikkandi/SMdRQA/releases)
[![image](https://img.shields.io/github/discussions/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA/discussions)
[![image](https://img.shields.io/github/created-at/SwaragThaikkandi/SMdRQA)](https://github.com/SwaragThaikkandi/SMdRQA)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/label.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/label.yml)
[![image](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/docs-build.yml/badge.svg)](https://github.com/SwaragThaikkandi/SMdRQA/actions/workflows/docs-build.yml)
